package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {
    public static void main(String[] args) {
        // Declare variables
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        // Instantiate Scanner object
        Scanner scanner = new Scanner(System.in);

        // Gather user's input
        System.out.print("Enter your first name: ");
        firstName = scanner.nextLine();

        System.out.print("Enter your last name: ");
        lastName = scanner.nextLine();

        System.out.print("Enter your grade in first subject: ");
        firstSubject = scanner.nextDouble();

        System.out.print("Enter your grade in second subject: ");
        secondSubject = scanner.nextDouble();

        System.out.print("Enter your grade in third subject: ");
        thirdSubject = scanner.nextDouble();

        // Get average grade
        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        // Display output
        System.out.println("Name: " + firstName + " " + lastName);
        System.out.println("Average grade: " + String.format("%.2f", average));
    }
}
